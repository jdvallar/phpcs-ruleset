<?php


namespace Vallarj\PhpCodeSniffer\VStandard\Sniffs\WhiteSpace;


use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Util\Tokens;

class SuperfluousWhitespaceSniff implements Sniff
{
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function register()
    {
        return [
            T_CLASS,
            T_INTERFACE,
            T_TRAIT,
            T_FUNCTION
        ];
    }

    /**
     * Processes this test, when one of its tokens is encountered.
     *
     * @param File $phpcsFile
     * @param int $stackPtr
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        if (!isset($tokens[$stackPtr]['scope_opener'])) {
            // Probably an interface or abstract method.
            return;
        }

        $scopeOpener = $tokens[$stackPtr]['scope_opener'];

        for ($firstContent = ($scopeOpener + 1); $firstContent < $phpcsFile->numTokens; $firstContent++) {
            $code = $tokens[$firstContent]['code'];

            if ($code === T_WHITESPACE
                || ($code == T_INLINE_HTML && trim($tokens[$firstContent]['content']) === '')
            ) {
                continue;
            }

            // Skip all empty tokens on the same line as the opener.
            if ($tokens[$firstContent]['line'] === $tokens[$scopeOpener]['line']
                && (isset(Tokens::$emptyTokens[$code]) === true || $code === T_CLOSE_TAG)
            ) {
                continue;
            }

            break;
        }

        if (isset($ignore[$tokens[$firstContent]['code']]) === false
            && $tokens[$firstContent]['line'] >= ($tokens[$scopeOpener]['line'] + 2)
        ) {
            $gap = ($tokens[$firstContent]['line'] - $tokens[$scopeOpener]['line'] - 1);
            $phpcsFile->recordMetric($stackPtr, 'Blank lines at start of structure', $gap);

            $error = 'The opening brace for the %s must not be followed by a whitespace.';
            $data = [$tokens[$stackPtr]['content']];
            $fix = $phpcsFile->addFixableError($error, $scopeOpener, 'WhitespaceAfterOpeningBrace', $data);

            if ($fix === true) {
                $phpcsFile->fixer->beginChangeset();
                $i = ($scopeOpener + 1);
                while ($tokens[$i]['line'] !== $tokens[$firstContent]['line']) {
                    // Start removing content from the line after the opener.
                    if ($tokens[$i]['line'] !== $tokens[$scopeOpener]['line']) {
                        $phpcsFile->fixer->replaceToken($i, '');
                    }

                    $i++;
                }

                $phpcsFile->fixer->endChangeset();
            }
        } else {
            $phpcsFile->recordMetric($stackPtr, 'Blank lines at start of structure', 0);
        }
    }
}